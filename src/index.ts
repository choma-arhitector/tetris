import './style/initial.scss';
import * as PIXI from 'pixi.js';

export class Index {
    protected _app = new PIXI.Application();
    protected _graphics = new PIXI.Graphics();

    protected _winLine: number = 0;

    protected _matrixPosition: any = {};

    constructor() {
        this.init();
    }

    protected init(): void {
        this._app = new PIXI.Application({
            width: 200, height: 600, backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1,
        });
        document.body.appendChild(this._app.view);
        document.addEventListener('keydown', this.initKeyboardEvents.bind(this));

        this.initMatrix();
        this.spawnGraphics();
    }

    protected initMatrix(): void {
        for (let j = 0; j < this._app.view.height / 20; j++) {
            this._matrixPosition[j] = [];
            for (let i = 0; i < this._app.view.width / 20; i++) {
                this._matrixPosition[j].push(0);
            }
        }
    }

    protected spawnGraphics(): void {
        this._graphics = new PIXI.Graphics();
        this._graphics.beginFill(0xDE3249);
        this._graphics.drawRect(0, 0, 20, 20);

        this._app.stage.addChild(this._graphics);

        this.motion();
    }

    protected motion(): void {
        const id = setInterval(() => {
            if (this.isNextStep()) {
                this.setMatrixPosition(this._graphics.x, this._graphics.y);
                clearInterval(id);
                this.spawnGraphics();
            } else {
                this._graphics.y += 20;
            }
        },100);
    }

    protected isNextStep(): boolean {
        const lastLine: number = 29;
        let posY = this._graphics.y / 20;
        let posX = this._graphics.x / 20;

        if (posY === lastLine) {
            return true;
        }

        return this._matrixPosition[posY + 1][posX] === 1;
    }

    protected setMatrixPosition(x: number, y: number): void {
        let posY = y / 20;
        let posX = x / 20;

        this._matrixPosition[posY][posX] = 1;

        for (let i = 0; i < this._matrixPosition[posY].length; i++) {
            if (this._matrixPosition[posY][i] === 1) {
                this._winLine++
            }
        }

        if (this._winLine === 10) {
            for (let i = 0; i < this._app.stage.children.length; i++) {
                if (this._app.stage.children[i].y === y) {
                    this._app.stage.removeChild(this._app.stage.children[i]);
                    i--;
                }
            }

            this.resetMatrixPosition(posY);
        }

        this.resetWinLine();
    }

    protected resetMatrixPosition(y: number): void {
        for (let i = 0; i < this._app.view.width / 20; i ++) {
            this._matrixPosition[y][i] = 0;
        }
    }

    protected resetWinLine(): void {
        this._winLine = 0;
    }

    protected initKeyboardEvents(key: any) {

            //move to the left
            if (key.keyCode === 37 && this._graphics.x != 0) {
                this._graphics.x -= 20;
            }
            //move to the right
            if (key.keyCode === 39  && this._graphics.x != 180) {
                this._graphics.x += 20;
            }
            //move to the down
            if (key.keyCode === 40) {
                this._graphics.y = 560;
            }
    }
}

new Index();