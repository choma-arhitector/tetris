const path = require('path');

let conf = {
    entry: './src/index.ts',
    devtool: 'sourcemap',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: "main.js",
        publicPath: "dist/"
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                    use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    devServer: {
        overlay: true
    }
};

module.exports = conf;